import axios from 'axios';
import { addPokemon } from './actions'

export const addPokemonThunk = (pokemonName, setError) => (dispatch) => {
    axios.get(`https://pokeapi.co/api/v2/pokemon/${pokemonName}`).then(res => {
       
        dispatch(addPokemon(res.data));
    
    }).catch((err) => setError(true));
}