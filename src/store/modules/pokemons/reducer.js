import { ADD_POKEMON } from './actionsTypes'

const pokemonReducer = (state = [], action) => {
    switch (action.type){
        case ADD_POKEMON:
            const { pokemon } = action
            return [...state, pokemon];
        default:
            return state;
    }
}

export default pokemonReducer;