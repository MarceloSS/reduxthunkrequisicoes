import './App.css';
import InputSearch from './components/inputsearch';
import Pokemons from './components/pokemons'

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <InputSearch/>
        <Pokemons/>
      </header>
    </div>
  );
}

export default App;
