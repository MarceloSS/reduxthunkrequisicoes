import { useSelector } from "react-redux";

const Pokemons = () => {
    const pokemons = useSelector((state) => state.pokemons);
    return (
        <div style={{display:'inline-block'}}>
            {pokemons.map((pokemon, index) => {
                return(
                    <div key={index} style={{padding:'5px'}}>
                        <div>
                            <img src={pokemon.sprites.front_default} alt={pokemon.name}/>  
                        </div>
                        <div>
                            {pokemon.name} 
                        </div>
                    </div>
                )
            })}
        </div>
    )
}

export default Pokemons