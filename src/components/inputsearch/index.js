import { useState } from 'react';
import { useDispatch } from 'react-redux';
import { addPokemonThunk } from '../../store/modules/pokemons/thunks'


const InputSearch = () => {
    const dispatch = useDispatch();

    const [input, setInput] = useState('');
    const [error, setError] = useState(false);

    const handleSearch = () => {
        setInput('')
        setError(false)
        dispatch(addPokemonThunk(input, setError))
    }

    return (
        <div>
            <input onChange={(event) => setInput(event.target.value)} value={input}/>
            <button onClick={ handleSearch }>Pesquisar</button>
            <div>
                {error && <span style={{color:'red'}}>Pokemon não encontrado</span>}
            </div>
        </div>
    )
}

export default InputSearch;